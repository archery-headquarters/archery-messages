from datetime import datetime
from click import secho


class Message:
    def __init__(
        self: object,
        module: str = 'Undefined'
    ) -> None:
        self.module = module

    def error(
        self: object,
        message: str
    ) -> str:
        color = 'red'
        self.show(
            color=color, message=message
        )

    def warning(
        self: object,
        message: str
    ) -> str:
        color = 'yellow'
        self.show(
            color=color, message=message
        )

    def success(
        self: object,
        message: str
    ) -> str:
        color = 'green'
        self.show(
            color=color, message=message
        )

    def info(
        self: object,
        message: str
    ) -> str:
        color = 'white'
        self.show(
            color=color, message=message
        )

    def show(
        self: object,
        color: str,
        message: str
    ) -> str:
        secho(
        	str(datetime.now()) +\
        	' - ' + self.module + ' - ' +\
        	message,
			fg=color, bold=True
		)