from click import command, option
from archery_messages import Message
from archery_exceptions import NullValueError

@command()
@option('--location', default='Test', help='Location.')
@option('--reason', default='info', help='Message reason.')
@option('--text', help='Message text.')
def cli(
    location: str, reason: str, text: str
) -> str:
    if text:
        message = Message(location)
        match reason:
            case 'error':
                message.error(text)
            case 'warning':
                message.warning(text)
            case 'success':
                message.success(text)
            case _:
                message.info(text)
    else:
        raise NullValueError('text')