# Archery Messages
Messages for the Archery ecosystem.

### How to use:

```python
from archery_messages import Message


message = Message(module='Test')
message.error('This is a error message.')
message.warning('This is a warning message.')
message.success('This is a success message.')
message.info('This is a info message.')
```

Is it possible to use it as a python module:

```bash
$ python -m archery-messages --help
Usage: python -m archery_messages [OPTIONS]

Options:
  --location TEXT  Location.
  --reason TEXT    Message reason.
  --text TEXT      Message text.
  --help           Show this message and exit.
```